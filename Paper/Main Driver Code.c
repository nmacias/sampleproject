#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>

/*
 * Driver for in-vivo orientation discovery
 * July 2013 - njm
 *
 * Connect to the sim running in -client mode; send S commands, read
 * responses and adapt...
 *
 */

// Global rotation data
int rots[128][128];

main()
{
  int i,done,rowDone,colDone,count;
  int width,height,saveWidth,saveHeight,r,c;
  char buffer[120];

  if (0 != socketSetup()) return; // failure of some sort
  sendStr("I\n");
  sendStr("I\nM 1 10\nu 128\n");

// ALL ORIENTATIONS ARE # OF CW TURNS...

// orient PC0 (cell [0,0])

  i=(-1); // last orientation
  while (0 == readOutput(0,0,"dw")){
    ++i;
    setInput(0,0,"cw",1);
    compSendO(0,0,"dw","dw=1",i);
    setInput(0,0,"cw",0);
  }

  printf("Got it! PC0 rot=%d\n",i);
  rots[0][0]=i;

// now find rotation of CC0 (cell [1,0])

  i=(-1);done=0;
  while (done==0){
    ++i; // try this rotation for CC0
// configure PC0 to configure CC0
    setInput(0,0,"cw",1);
    compSendO(0,0,"dw","cs=1;ds=w",rots[0][0]); // we know its orientation
    setInput(0,0,"cw",0);
    compSendO(0,0,"dw","dn=1",i); // try to send a 1 to/through PC0
    setInput(0,0,"cw",1);
    compSendO(0,0,"dw","ds=w;dw=s",rots[0][0]);
    setInput(0,0,"cw",0);
    done=readOutput(0,0,"dw");
  }

  printf("CC0 rot=%d\n",i);
  rots[1][0]=i;

// Build standard PC/CC head cell pair

  setInput(0,0,"cw",1);
  compSendO(0,0,"dw","cs=1;ds=w",rots[0][0]);
  setInput(0,0,"cw",0);
  compSendO(0,0,"dw","dw=n;dn=we~&;de=we&",rots[1][0]);
  setInput(0,0,"cw",1);
  compSendO(0,0,"dw","ds=1;ce=s;de=w;dw=e",rots[0][0]);
  setInput(0,0,"cw",0);

// now loop across the top two rows
  rowDone=0;
  count=0;
  while (!rowDone){
    ++count; // find the width of the matrix too :)

// orient PC_count (cell [0,count])
    i=(-1); // last orientation

    while ((rowDone==0) && (0 == readOutput(0,0,"dw"))){
      ++i;
      setInput(1,0,"dw",1);
      compSendO(0,0,"dw","dw=1",i);
      setInput(1,0,"dw",0);
      if (i>4) rowDone=1;
    }
  
    if (rowDone){printf("Row is finished; count=%d\n",count);} else {

      printf("Got it! PC%d rot=%d\n",count,i);
      rots[0][count]=i;
  
  // now find rotation of CC_count (cell [1,count])
  
      i=(-1);done=0;
      while (done==0){
        ++i; // try this rotation for CC_count
    // configure PC_count to configure CC_count
        setInput(1,0,"dw",1);
        compSendO(0,0,"dw","cs=1;ds=w",rots[0][count]);
        setInput(1,0,"dw",0);
  // Here we can't just set dn=1...if this gets sent to the west,
  // we'll move the head of the wire. So use DN=N. If the cell is
  // mis-oriented, N_in will never be asserted so DN_out will remain low
  // :)
        compSendO(0,0,"dw","dn=n",i);
        setInput(1,0,"dw",1);
        compSendO(0,0,"dw","ds=w;dw=s",rots[0][count]);
        setInput(1,0,"dw",0);
        setInput(0,0,"dw",1); // send the ping
        done=readOutput(0,0,"dw"); // and read the echo
        setInput(0,0,"dw",0); // probably not really necessary
      }
    
      printf("CC%d rot=%d\n",count,i);
      rots[1][count]=i;
  
  sendStr("#extending wire\n");
  // extend the wire
      setInput(1,0,"dw",1);
      compSendO(0,0,"dw","cs=1;ds=w;ce=1",rots[0][count]); // now with PRECLEAR!
      setInput(1,0,"dw",0);
//if (count==7) return;
      compSendO(0,0,"dw","dw=n;dn=we~&;de=we&",rots[1][count]);
//if (count==7) return;
      setInput(1,0,"dw",1);
      compSendO(0,0,"dw","ds=1;ce=s;de=w;dw=e",rots[0][count]);
      setInput(1,0,"dw",0);
  sendStr("#Done extending wire\n");
    } // end of IF ROW NOT DONE
  } // done with top row!

// top two rows done :)

  saveWidth=width=count; // # of cells across

//
// We've analyzed the first two rows. Now we make a series of
// west/turn/south sweeps, analyzing pairs of cells on each southern sweep.
// "width" tells us how far to move (width-2). So we'll use width
// to loop over each pair of columns :)
//

// Now loop through each pair of columns, from right to left
  while (width >= 2){

// Okay, now we start the the upper-left again; move across
// till we're two spots from the eastern edge; turn CW; and
// travel south until we hit the southern edge of the matrix.

// Build standard PC/CC head cell pair and extend to width-2

    for (i=0;i<width-2;i++){
      assertCC(i,1); // if i=0 then set [0,0]cw else set [1,0]dw
      compSendO(0,0,"dw","cs=1;ds=w;ce=1",rots[0][i]); // preclear
      assertCC(i,0);
      compSendO(0,0,"dw","dw=n;dn=we~&;de=we&",rots[1][i]);
      assertCC(i,1); // if i=0 then set [0,0]cw else set [1,0]dw
      compSendO(0,0,"dw","ds=1;ce=s;de=w;dw=e",rots[0][i]);
      assertCC(i,0); // if i=0 then set [0,0]cw else set [1,0]dw
    }

// turn the corner
    i=width-2; // corner will be in columns i and i+1
    assertCC(width-2,1);
    compSendO(0,0,"dw","de=w;ce=1;cs=1",rots[0][i]);
    assertCC(width-2,0);
    compSendO(0,0,"dw","cs=1",rots[0][i+1]);
    assertCC(width-2,1);
    compSendO(0,0,"dw","de=w;ce=1",rots[0][i]);
    assertCC(width-2,0);
    compSendO(0,0,"dw","cs=1;ds=ws+",rots[0][i+1]);
    assertCC(width-2,1);
    compSendO(0,0,"dw","de=w",rots[0][i]);
    assertCC(width-2,0);
    compSendO(0,0,"dw","cs=w;dw=1;ds=n;dn=s",rots[1][i+1]); // outer corner
    assertCC(width-2,1);
    compSendO(0,0,"dw","de=w;ce=1",rots[0][i]);
    assertCC(width-2,0);

/*** outer corner pre-clear...unnecessary? ***

    compSendO(0,0,"dw","ce=1",rots[0][i+1]); // ready to pre-clear old corner
    assertCC(width-2,1); // pre-clear goes active
    compSendO(0,0,"dw","de=w;ce=1",rots[0][i]);
    assertCC(width-2,0);

***/

    compSendO(0,0,"dw","ds=w;dw=s",rots[0][i+1]); // corner[0,1]
    assertCC(width-2,1);
    compSendO(0,0,"dw","ds=w;cs=1",rots[0][i]);
    assertCC(width-2,0);
    compSendO(0,0,"dw","dw=n;ds=ws&;de=ws~&",rots[1][i]); // corner [1,0]
    assertCC(width-2,1);
    compSendO(0,0,"dw","dw=e;de=w;ds=1",rots[0][i]); // corner[0,0]
    assertCC(width-2,0);
    compSendO(0,0,"dw","",0);



// find orientation of two columns
    count=2; // # of rows
    colDone=0;

    while (colDone==0){
      i=(-1); // last orientation
      while ((colDone==0) &&
             ((0 == readOutput(0,0,"dw")) || (i == (-1)))){
// skip read from previous pass (i==(-1))
        ++i;
        setInput(1,0,"dw",1);
        compSendO(0,0,"dw","dn=n",i); // we're inside live cells, so be conservative
        setInput(1,0,"dw",0);
        setInput(0,0,"dw",1); // ping
        if (i>4) colDone=1;
      }
      setInput(0,0,"dw",0);
    
      if (colDone){height=count;printf("Column is finished; count=%d\n",count);} else {

        printf("[%d,%d]rot=%d\n",count,width-1,i);
        rots[count][width-1]=i;
    
    // now find rotation of CC_count (cell [count,width-2])
    
        i=(-1);done=0;
        while (done==0){
          ++i; // try this rotation for CC
      // configure PC to configure CC
          setInput(1,0,"dw",1);
          compSendO(0,0,"dw","cw=1;dw=n",rots[count][width-1]);
          setInput(1,0,"dw",0);
// configure CC
          compSendO(0,0,"dw","de=e",i);
          setInput(1,0,"dw",1);
          compSendO(0,0,"dw","dw=n;dn=w",rots[count][width-1]);
          setInput(1,0,"dw",0);
          setInput(0,0,"dw",1); // send the ping
          done=readOutput(0,0,"dw"); // and read the echo
          setInput(0,0,"dw",0); // probably not really necessary
        }
      
        printf("[%d,%d] rot=%d\n",count,width-2,i);
        rots[count][width-2]=i;
    
    sprintf(buffer,"#Extending wire, count=%d\n",count);
    sendStr(buffer);
    // extend the wire
        setInput(1,0,"dw",1);
        compSendO(0,0,"dw","cw=1;dw=n;cs=1",rots[count][width-1]);
        setInput(1,0,"dw",0);
        compSendO(0,0,"dw","dn=e;de=ns~&;ds=ns&",rots[count][width-2]);
        setInput(1,0,"dw",1);
        compSendO(0,0,"dw","dw=1;cs=w;ds=n;dn=s",rots[count][width-1]);
        setInput(1,0,"dw",0);
      } // end of COL NOT DONE
      ++count; // move to next row
    } // end of column
    if (1==width%2) ++width; // want to align on an even column
    width=width-2; // move left
  }

  saveHeight=height;

  printf("Orientation analysis complete!\n");
  printf("Matrix is %dx%d\n",saveHeight,saveWidth);

  printf("Orientation:\n");
  for (r=0;r<saveHeight;r++){
    for (c=0;c<saveWidth;c++){
      printf("%d",rots[r][c]);
    }
    printf("\n");
  }

  closeSocket(); // clean up
}


// column 0 is special: to configure cell [0,0] we need to assert CWin
// whereas other cells along the PC are configured by asserting [1,0]DWin
// assertCC(flag,val) handles this for us.
// If flag=0, we use [0,0]CWin else we use [1,0]DWin
// value is the value we set the input to

assertCC(flag,value)
int flag,value;
{
      if (flag==0) setInput(0,0,"cw",value);
              else setInput(1,0,"dw",value);
}

// compile and send a string
compSend(row,col,side,tt)
int row,col;
char *side,*tt;
{
  char buffer[120];
  sendStr("c\n");
  sendStr(tt);
  sendStr("\n.\n");
  sprintf(buffer,"s %d %d %s m\n",row,col,side);
  sendStr(buffer);
}

// rotated compSend
compSendO(row,col,side,ttIn,rots) // assuming rotation=rots (# of CW turns)
int row,col,rots;
char *side,*ttIn;
{
  int i,j;
  char tt[120];

  strcpy(tt,ttIn);
  for (i=0;i<4-rots;i++){ // totate tt
    for (j=0;j<strlen(tt);j++){
      switch(tolower(tt[j])){
        case 'e': tt[j]='s';break;
        case 's': tt[j]='w';break;
        case 'w': tt[j]='n';break;
        case 'n': tt[j]='e';break;
      }
    }
  }
  compSend(row,col,side,tt);
}


setInput(row,col,side,val)
int row,col,val;
char *side;
{
  char buffer[120];
  sprintf(buffer,"s %d %d %s %d\n",row,col,side,val);
  sendStr(buffer);
}


int mySock; // this is how we communicate with the sim

readOutput(row,col,side)
int row,col;
char *side;
{
  char buf[32],buf1[32],buf2[32],bufc[32],bufd[32];
  int i;

  sprintf(buf,"o %d %d\n",row,col);
  sendStr(buf);
  readStr(buf1);readStr(bufc);readStr(buf2);readStr(bufd);
//printf("got <%s> <%s> <%s> <%s>\n",buf1,bufc,buf2,bufd);
// pick the right string of bits
  switch (tolower(side[0])){
    case 'd': strcpy(buf,bufd);break;
    case 'c': strcpy(buf,bufc);break;
    default: printf("Error: side should be \"d\" or \"c\" but you asked for \"%s\"\n",side);
             return(0);
  }

// now pick the right return value
  switch (tolower(side[1])){
    case 'n': i=0;break;
    case 'e': i=1;break;
    case 's': i=2;break;
    case 'w': i=3;break;
    default: printf("Don't understand side \"%s\"\n",side);
             return(0);
  }
// now return correct value
  return((buf[i+2]=='1')?1:0);
}

readStr(buffer)
char *buffer;
{
  int i;
  read(mySock,buffer,1);

  i=0;
  while (buffer[i] != '\n'){
    read(mySock,&buffer[++i],1);
  }
  buffer[i]='\0'; // no NL
  return;
}

// all socket initialization
socketSetup()
{
  int status;
  struct sockaddr_in addr;	// address (us)

  mySock=socket(AF_INET,SOCK_STREAM,0);
  //printf("mySock=%x\n",mySock);

  addr.sin_family=AF_INET;
  addr.sin_port=htons(1204); // magic :)
  status=inet_pton(AF_INET,"127.0.0.1",&addr.sin_addr);
  //printf("inet_pton returns %d\n",status);

  status=connect(mySock,(struct sockaddr *) &addr,sizeof(addr));
  if (status != 0){
    printf("Error: connect() returned %d\n",status);
    return(1); // error
  }

  return(0); // success!
}

sendStr(char *str)
{
  write(mySock,str,strlen(str));
}

closeSocket()
{
  close(mySock);
}
