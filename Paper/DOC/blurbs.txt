It should be noted that this is the exact same cell design that was originally specified in 1986.

If you're going to add things to the system, then sure, it can do anything you want.

The trick is to design a system that will be able to do things in the future that you haven't yet imagined today.

This is the case, not because the cell design is so complex that it includes every possible embellishment; but because the cell design is so simple that it can be applied in infinite variety.
