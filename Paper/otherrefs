@inproceedings{biltrans,
  author = {Walker, James Alfred and Hilder, James A. and Tyrrell, Andy M.},
  booktitle = {ICES},
  editor = {Tempesti, Gianluca and Tyrrell, Andy M. and Miller, Julian F.},
  isbn = {978-3-642-15322-8},
  keywords = {dblp},
  pages = {1-12},
  publisher = {Springer},
  series = {Lecture Notes in Computer Science},
  timestamp = {2010-12-13T00:00:00.000+0100},
  title = {Measuring the Performance and Intrinsic Variability of Evolved Circuits.},
  volume = 6274,
  year = 2010
}


@Article{multicore,
  Title={Intel shows off 80-core processor},
  Author={Krazit, Tom},
  Journal={CNET News, Processors \& Semiconductors},
  Year={2011}
}

@article{mcadv,
 author = {Cooper, Keith D.},
 title = {Making Effective Use of Multicore Systems A Software Perspective: The Multicore Transformation (Ubiquity Symposium)},
 journal = {Ubiquity},
 issue_date = {September 2014},
 volume = {2014},
 number = {September},
 month = sep,
 year = {2014},
 issn = {1530-2180},
 pages = {3:1--3:8},
 articleno = {3},
 numpages = {8},
 publisher = {ACM},
 address = {New York, NY, USA},
} 

@Article{xilinx,
  Author = {Xilinx Inc.},
  Title = {Xilinx Ships World's Highest Capacity FPGA and Shatters Industry Record for Number of Transistors by 2X},
  Journal = {Press Release, PR Newswire},
  Year = 2011
}

@article{Moore65,
  author = {Moore, Gordon E.},
  journal = {Electronics},
  keywords = {1965 Moore},
  month = {April},
  number = 8,
  timestamp = {2011-05-23T17:50:46.000+0200},
  title = {Cramming more components onto integrated circuits},
  volume = 38,
  year = 1965
}

@inproceedings{threed,
  added-at = {2012-04-18T00:00:00.000+0200},
  author = {Kim, Dae Hyun and Athikulwongse, Krit and Healy, Michael B. and Hossain, Mohammad and Jung, Moongon and Khorosh, Ilya and Kumar, Gokul and Lee, Young-Joon and Lewis, Dean L. and Lin, Tzu-Wei and Liu, Chang and Panth, Shreepad and Pathak, Mohit and Ren, Minzhen and Shen, Guanhao and Song, Taigon and Woo, Dong Hyuk and Zhao, Xin and Kim, Joungho and Choi, Ho and Loh, Gabriel H. and Lee, Hsien-Hsin S. and Lim, Sung Kyu},
  booktitle = {ISSCC},
  isbn = {978-1-4673-0376-7},
  keywords = {dblp},
  pages = {188-190},
  publisher = {IEEE},
  timestamp = {2012-04-18T00:00:00.000+0200},
  title = {3D-MAPS: 3D Massively parallel processor with stacked memory.},
  year = 2012
}

@article{eblocks1,
  title={Forming electrical networks in three dimensions by self-assembly},
  author={Gracias, David H and Tien, Joe and Breen, Tricia L and Hsu, Carey and Whitesides, George M},
  journal={Science},
  volume={289},
  number={5482},
  pages={1170--1172},
  year={2000},
  publisher={American Association for the Advancement of Science}
}

@inproceedings{dna,
  title={Circuit and system architecture for DNA-guided self-assembly of nanoelectronics},
  author={Patwardhan, Jaidev P and Dwyer, Chris and Lebeck, Alvin R and Sorin, Daniel J},
  booktitle={Foundations of Nanoscience: Self-Assembled Architectures and Devices},
  pages={344--358},
  year={2004}
}

@book{CA,
  title={Cellular automata and complexity: collected papers},
  author={Wolfram, Stephen},
  volume={1},
  year={1994},
  publisher={Addison-Wesley Reading}
}

@book{systolic,
  title={Systolic array},
  author={Kung, HT},
  year={2003},
  publisher={John Wiley and Sons Ltd.}
}

@article{J2001,
  title={The cell matrix: an architecture for nanocomputing},
  author={Durbeck, Lisa JK and Macias, Nicholas J},
  journal={Nanotechnology},
  volume={12},
  number={3},
  pages={217},
  year={2001},
  publisher={IOP Publishing}
}

@inproceedings{CMFT,
  title={Defect-tolerant, fine-grained parallel testing of a Cell Matrix},
  author={Durbeck, Lisa J and Macias, Nicholas J},
  booktitle={ITCom 2002: The Convergence of Information Technologies and Communications},
  pages={71--85},
  year={2002},
  organization={International Society for Optics and Photonics}
}

@phdthesis{NMdiss,
  title={Self-Modifying Circuitry for Efficient, Defect-Tolerant Handling of Trillion-element Reconfigurable Devices},
  author={Macias, Nicholas J},
  year={2011},
  school={Virginia Polytechnic Institute and State University}
}

@inproceedings{serialconfig,
  title={Open-Source Bitstream Generation},
  author={Soni, Ritesh Kumar and Steiner, Neil and French, Matthew},
  booktitle={Field-Programmable Custom Computing Machines (FCCM), 2013 IEEE 21st Annual International Symposium on},
  pages={105--112},
  year={2013},
  organization={IEEE}
}

@inproceedings{nanoarch,
  title={A cellular architecture for self-assembled 3D computational devices},
  author={Macias, NJ and Pandey, S and Deswandikar, A and Kothapalli, CK and Yoon, CK and Gracias, DH and Teuscher, C},
  booktitle={Nanoscale Architectures (NANOARCH), 2013 IEEE/ACM International Symposium on},
  pages={116--121},
  year={2013},
  organization={IEEE}
}

@article{magalign,
  title={Aligned Nanostructured Polymers by Magnetic-Field-Directed Self-Assembly of a Polymerizable Lyotropic Mesophase},
  author={Tousley, Marissa E and Feng, Xunda and Elimelech, Menachem and Osuji, Chinedum O},
  journal={ACS applied materials \& interfaces},
  year={2014},
  publisher={ACS Publications}
}

@incollection{latestbook,
  title={Self-organizing computing systems: songline processors},
  author={Macias, Nicholas J and Durbeck, Lisa JK},
  booktitle={Advances in applied self-organizing systems},
  pages={211--262},
  year={2013},
  publisher={Springer}
}

@inproceedings{macias2007,
  title={Application of self-configurability for autonomous, highly-localized self-regulation},
  author={Macias, Nicholas J and Athanas, Peter M},
  booktitle={Adaptive Hardware and Systems, 2007. AHS 2007. Second NASA/ESA Conference on},
  pages={397--404},
  year={2007},
  organization={IEEE}
}

@Article{paralleldiff,
  Title={Massively-Parallel Differentiation for Efficient Handling of Cellular Disorientation},
  Author={Macias, Nicholas},
  Year={Manuscript in preparation}
}

@article{bootstrap,
  title={The system design of the IBM Type 701 computer},
  author={Buchholz, Werner},
  journal={Proceedings of the IRE},
  volume={41},
  number={10},
  pages={1262--1275},
  year={1953},
  publisher={IEEE}
}

@article{selfassem,
  title={Self-assembly at all scales},
  author={Whitesides, George M and Grzybowski, Bartosz},
  journal={Science},
  volume={295},
  number={5564},
  pages={2418--2421},
  year={2002},
  publisher={American Association for the Advancement of Science}
}

 "Sequoia sempervirens (D. Don) Endlicher". www.efloras.org: Flora of North America. Retrieved 1 February 2015.

@misc{redwood,
  title = {{Sequoia sempervirens in Flora of North America}},
  howpublished = {\url{http://www.efloras.org}},
  note = {Accessed: 2015-05-22}
}

@article{humanSA,
  title={Dissections: Self-assembled aggregates that spontaneously reconfigure their structures when their environment changes},
  author={Mao, Chengde and Thalladi, Venkat R and Wolfe, Daniel B and Whitesides, Sue and Whitesides, George M},
  journal={Journal of the American Chemical Society},
  volume={124},
  number={49},
  pages={14508--14509},
  year={2002},
  publisher={ACS Publications}
}

@incollection{tourSA,
  title={Self-Assembled Molecular Electronics},
  author={James, Dustin K and Tour, James M},
  booktitle={Nanoscale Assembly},
  pages={79--98},
  year={2005},
  publisher={Springer}
}

@article{graciasSA,
  title={Surface tension-driven self-folding polyhedra},
  author={Leong, Timothy G and Lester, Paul A and Koh, Travis L and Call, Emma K and Gracias, David H},
  journal={Langmuir},
  volume={23},
  number={17},
  pages={8747--8751},
  year={2007},
  publisher={ACS Publications}
}

@article{chemdirectsa,
  title={Self-assembled, deterministic carbon nanotube wiring networks},
  author={Diehl, Michael R and Yaliraki, Sophia N and Beckman, Robert A and Barahona, Mauricio and Heath, James R},
  journal={Angewandte Chemie International Edition},
  volume={41},
  number={2},
  pages={353--356},
  year={2002},
  publisher={Wiley Online Library}
}
